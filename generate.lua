package.path = package.path..[[;./script/?.lua;./data/?.lua;./data/base/?.lua;;./data/core/?.lua;./data/core/lualib/?.lua]]
require("dataloader")
data.raw['noise-expression'] = {}
data.raw['gui-style'] = {default={}}
options = {}
require("defines")
require("core/data")
require("base/data")
require("base/data-updates")
require("wiki-util")
local recipeData = dealRecipe(table.deepCopy(data.raw.recipe))
writeLua("recipe", {"recipe"}, recipeData)
--文件分组列表
local file = {}
--文件排除列表
local dataExcludeKeys = {"noise-expression", "explosion", "tile", "resource", "optimized-particle", "corpse", "gui-style", "optimized-decorative", "tree", "turret"}
for fileIndex, fileKeys in ipairs(file) do
    local fileValue = {}
    for fileKeyIndex, fileKey in ipairs(fileKeys) do
        if(data.raw[fileKey]~=nil)
        then
            table.insert(dataExcludeKeys, fileKey)
            fileValue[fileKey] = data.raw[fileKey]
         end
    end
    writeJson('wiki/'..fileIndex, 'json', fileValue)
end

local restFileKeys = {}
local restFileValue = {}
for dataRawKey, dataRawValue in pairs(data.raw) do
    writeJson('other/'..dataRawKey, 'json', dataRawValue)
    if(not isInclude(dataExcludeKeys, dataRawKey))
    then
        table.insert(restFileKeys, dataRawKey)
        restFileValue[dataRawKey] = dataRawValue
    end
end
writeJson('wiki/rest', 'json', restFileValue)
writeJson('wiki/factorio-data', 'doc', restFileValue)

local iconTranslate = {}
local itemWithIcons = {}
local itemWithIcons2 = {}
for dataRawKey, dataRawValue in pairs(data.raw) do
    for itemKey, itemValue in pairs(dataRawValue) do
        if itemValue.icons ~= nil
        then
            itemWithIcons2[itemKey] = itemValue.icons
            itemWithIcons[itemKey] = {}
            for index, oneIcon in ipairs(itemValue.icons) do
                local iconParts = string.split(oneIcon.icon, '/')
                local iconFileName = iconParts[#iconParts]
                local iconName = string.sub(iconFileName, 1, string.len(iconFileName)-4)
                itemWithIcons[itemKey][iconName] = oneIcon
            end
        end
        if dataRawKey ~= "technology"  and dataRawKey ~= "explosion"
         and itemValue.icon ~= nil and type(itemValue.icon)== "string"
        then
            local iconParts = string.split(itemValue.icon, '/')
            local iconFileName = iconParts[#iconParts]
            local iconName = string.sub(iconFileName, 1, string.len(iconFileName)-4)
            if itemKey ~= iconName then iconTranslate[itemKey] = iconName end
        end
    end
end
writeLua("iconTranslate", {"iconTranslate"}, iconTranslate)
writeLua("itemWithIcons", {"itemWithIcons"}, itemWithIcons)
writeJson('itemWithIcons', 'json', itemWithIcons)
writeJson('itemWithIcons2', 'json', itemWithIcons2)

local localeFile  = io.open("data/base/locale/zh-CN/base.cfg" ,"r");
local localeLines = {}
for line in localeFile:lines() do
	table.insert(localeLines, line)
end
local translateToCNTable = {}
local translateToEnTable = {}
local du = {}
local partName
for i0, localeLine in ipairs(localeLines) do
    local midIndex = string.find(localeLine,"=")
    if midIndex == nil
    then
        partName = string.gsub(localeLine, "[%[%]]", "")
    elseif partName ~= nil
    then

            if translateToCNTable[partName] == nil then translateToCNTable[partName] = {} end
            if translateToEnTable[partName] == nil then translateToEnTable[partName] = {} end
            local left = string.lower(string.sub(localeLine, 1, midIndex-1))
            local right = string.lower(string.sub(localeLine, midIndex+1, -1))
            translateToCNTable[partName][left] = right
            translateToEnTable[partName][right] = left
            if du[right] == nil then du[right] = {} end
            table.insert(du[right],left)
        if partName~="entity-description" and partName ~= "technology-description" and partName ~= "achievement-description" and partName ~= "item-description"
        and partName ~= "tutorial-description" and partName ~= "mod-description" and partName ~= "map-gen-preset-description" and partName ~= "virtual-signal-description"
        and partName ~= "autoplace-control-names" and partName ~= "tile-name" and partName ~= "technology-name" and partName ~= "control" and partName ~= "shortcut" and partName ~= "damage-type-name"
        and partName ~= "ammo-category-name" and partName ~= "entity-name" and partName ~= "decorative-name" and partName ~= "fuel-category-name" and partName ~= "equipment-name"
        then
        end
    end
end
writeLua("translateToCN", {"translateToCN"}, translateToCNTable)
writeLua("translateToEN", {"translateToEN"}, translateToEnTable)
writeJson('translateToCN', 'json', translateToCNTable)
writeJson('translateToEN', 'json', translateToEnTable)
writeJson('wiki/factorio-translate', 'doc', translateToCNTable)

for dux, duy in pairs(du) do
    if #duy == 1 then du[dux] = nil end
end
writeJson('du', 'json', du)
