JSON = (loadfile "script/JSON.lua")()
string.split = function(s, p)
    local rt= {}
    print("-"..s.."-")
    string.gsub(s, '[^'..p..']+', function(w) table.insert(rt, w) end )
    return rt
end
table.deepCopy = function(object)
	local lookup_table = {}
	local function _copy(object)
		if type(object) ~= "table" then return object
	-- don't copy factorio rich objects
		elseif object.__self then return object
		elseif lookup_table[object] then return lookup_table[object]
		end
		local new_table = {}
		lookup_table[object] = new_table
		for index, value in pairs(object) do
			new_table[_copy(index)] = _copy(value)
		end
		return setmetatable(new_table, getmetatable(object))
	end
	return _copy(object)
end

function isInclude(table, value)
    for k,v in ipairs(table) do
      if v == value then
          return true
      end
    end
    return false
end

--字符串写入
function writeFile(fileName,string)
    local f = assert(io.open(fileName, 'w'))
    f:write(string)
    f:close()
    print(fileName)
end
function createLuaString(fileKeys, fileValue)
    local luaString = require("pl.pretty").write(fileValue,'')
    luaString = string.gsub(luaString, "Inf", 999999999)
    luaString = string.gsub(luaString, "%[=%[", "\"")
    luaString = string.gsub(luaString, "%]=%]", "\"")
    return luaString
end
function writeLua(fileName, fileKeys, fileValue)
    local fileKeyString = "-- "
    for i, key in ipairs(fileKeys) do fileKeyString = fileKeyString..key..',' end
    local luaString = createLuaString(fileKeys, fileValue)
    writeFile(fileName..'.lua',fileKeyString..'\n'..'return\n'..luaString)
end
function writeJson(fileName, suffix, fileValue)
    print(fileName)
    require("pl.file").write(fileName..'.'..suffix,JSON:encode(fileValue))
end
function dealRecipe(recipeData)
    local newRecipeData = {}
    local function formatRecipeInfo(itemInfo)
            local newIngredients = {}
            for i0, ingre in ipairs(itemInfo.ingredients) do
                local name = ingre.name or ingre[1]
                newIngredients[name] ={amount = ingre.amount or ingre[2] or 0, type = ingre.type or "item" }
            end
            itemInfo.ingredients = newIngredients
            itemInfo.energy_required = itemInfo.energy_required or 0.5
            itemInfo.result_count = itemInfo.result_count or 1
            itemInfo.category = itemInfo.category or "crafting"
            return itemInfo
        end
    for recipeKey, recipeValue in pairs(recipeData) do
        recipeValue.type = nil
        recipeValue.name = nil
        recipeValue.order = nil
        recipeValue.icon = nil
        recipeValue.icon_mipmaps = nil
        recipeValue.icon_size = nil
        recipeValue.crafting_machine_tint = nil
        if recipeValue.normal ~=nil and recipeValue.expensive~=nil
        then
            recipeValue.normal.category = recipeValue.category or "crafting"
            recipeValue.normal = formatRecipeInfo(recipeValue.normal)
            recipeValue.expensive.category = recipeValue.category or "crafting"
            recipeValue.expensive = formatRecipeInfo(recipeValue.expensive)
        else
            recipeValue.category = recipeValue.category or "crafting"
            recipeValue = formatRecipeInfo(recipeValue)
        end
        newRecipeData[recipeKey] = recipeValue
    end
    return newRecipeData
end



